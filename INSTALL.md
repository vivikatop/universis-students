# UniverSIS-students Installation

## Prerequisites

Node.js version >6.14.3 is required. Visit [Node.js](https://nodejs.org/en/)
and follow the installation instructions provided for your operating system.

## Install dependencies

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.8. To **install angular-cli** execute

`npm install -g @angular/cli`

You may need to run it as root.


Navigate to application directory and execute:

`npm i`

## Deploy
### Development

1. Run `ng serve` for a devevelopment server
- Navigate to `http://localhost:7001/`

The app will automatically reload if you change any of the source files.

### Production
1. Configuration

  `cp src/assets/config/app.json src/assets/config/app.production.json`

  Add the appropriate values for client_id, client_secret and URLs in `src/assets/config/app.production.json`

- Make sure you build from the source code after every change

    `npm run build`

- Use pm2 to manage the application
  * start

    `pm2 start pm2.config.json`

    Navigate to `http://localhost:7001/`

  * check the status

    `pm2 status universis_students`

  * stop

    `pm2 stop universis_students`
