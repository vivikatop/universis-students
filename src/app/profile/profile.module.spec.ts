import { ProfileModule } from './profile.module';
import {async, TestBed} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TranslateModule} from '@ngx-translate/core';
import {MostModule} from '@themost/angular';
// important: exclude test because of a karma error on linux
// todo: fix error on linux
xdescribe('ProfileModule', () => {
  let profileModule: ProfileModule;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        TranslateModule.forRoot(),
        MostModule.forRoot({
          base: '/',
          options: {
            useMediaTypeExtensions: false
          }
        })
      ]
    });
  }));

  it('should create an instance', () => {
    profileModule = new ProfileModule();
    expect(profileModule).toBeTruthy();
  });
});
