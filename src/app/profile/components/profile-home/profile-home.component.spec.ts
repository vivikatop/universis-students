import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {LocalizedDatePipe} from 'src/app/shared/localized-date.pipe';

import {ProfileHomeComponent} from './profile-home.component';
import {ProfilePreviewComponent} from '../profile-preview/profile-preview.component';
import {SharedModule} from 'src/app/shared/shared.module';
import {UserService} from 'src/app/auth/services/user.service';
import {MostModule} from '@themost/angular';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {ConfigurationService} from 'src/app/shared/services/configuration.service';
import {TranslateModule} from '@ngx-translate/core';
import {ProfileService} from '../../services/profile.service';
import {BsModalService, ComponentLoaderFactory, ModalModule, PositioningService} from 'ngx-bootstrap';
import {TestingConfigurationService} from '../../../test';
import {LoadingService} from '../../../shared/services/loading.service';
import {RouterTestingModule} from '@angular/router/testing';

describe('ProfileHomeComponent', () => {
    // tslint:disable-next-line:prefer-const
    let component: ProfileHomeComponent;
    // tslint:disable-next-line:prefer-const
    let fixture: ComponentFixture<ProfileHomeComponent>;

    beforeEach(async(() => {
        return TestBed.configureTestingModule({
            declarations: [ProfileHomeComponent,
                ProfilePreviewComponent],
            imports: [
                ModalModule,
                RouterTestingModule,
                HttpClientTestingModule,
                TranslateModule.forRoot(),
                MostModule.forRoot({
                    base: '/',
                    options: {
                        useMediaTypeExtensions: false
                    }
                }),
                SharedModule],
            providers: [
                {
                    provide: ConfigurationService,
                    useClass: TestingConfigurationService
                },
                BsModalService,
                ProfileService,
                UserService,
                LoadingService,
                ComponentLoaderFactory,
                PositioningService
            ]
        }).compileComponents();
    }));

});
