import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {UserService} from '../../../auth/services/user.service';
import {ProfileService} from '../../services/profile.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {MostModule} from '@themost/angular';
import {ConfigurationService} from 'src/app/shared/services/configuration.service';
import {TranslateModule} from '@ngx-translate/core';

import {ProfileCardComponent} from './profile-card.component';
import {TestingConfigurationService} from '../../../test';

describe('ProfileCardComponent', () => {
    let component: ProfileCardComponent;
    let fixture: ComponentFixture<ProfileCardComponent>;

    beforeEach(async(() => {
        return TestBed.configureTestingModule({
            declarations: [
                ProfileCardComponent
            ],
            imports: [
                HttpClientTestingModule,
                TranslateModule.forRoot(),
                MostModule.forRoot({
                    base: '/',
                    options: {
                        useMediaTypeExtensions: false
                    }
                })],
            providers: [
                {
                    provide: ConfigurationService,
                    useClass: TestingConfigurationService
                },
                ProfileService,
                UserService
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ProfileCardComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
