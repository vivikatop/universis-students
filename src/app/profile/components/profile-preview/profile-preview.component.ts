import {Component, OnInit} from '@angular/core';
import { UserService } from "../../../auth/services/user.service";
import { AngularDataContext } from '@themost/angular';
import { ProfileService } from '../../services/profile.service';
import { LoadingService} from '../../../shared/services/loading.service';


@Component({
    selector: 'app-profile-preview',
    templateUrl: 'profile-preview.component.html',
    styleUrls: ['profile-preview.component.scss']
})

export class ProfilePreviewComponent implements OnInit {

    // stdent object coming from the API
    public student: any;

    public loading = true;   // Only if data is loaded

    constructor(private _userService: UserService,
        private _profileService: ProfileService,
        private loadingService: LoadingService) {
    }

    ngOnInit() {
        this.loadingService.showLoading();  // show loading
        this._profileService.getStudent().then(res => {
            this.student = res; // Load data
            this.loading = false; // Data is loaded
            this.loadingService.hideLoading(); // hide loading
        });
    }
}
