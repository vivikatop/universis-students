import { Component, Input, OnInit } from '@angular/core';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'card-collapsible',
  template: `
    <div class="card mb-0">
      <div class="card-header bg-white border-0">
        <h5 class="mb-0 mt-2 text-dark font-weight-normal">
          <ng-content *ngIf="!alternateHeader || (alternateHeader && collapsed)" select="card-header"></ng-content>
          <ng-content *ngIf="!collapsed && alternateHeader" select="card-alternate-header"></ng-content>
        </h5>
        <button (click)="collapsed = !collapsed"
                class="d-inline-block btn btn-link collapsed float-right text-secondary text-decoration-none"
                type="button">
          <i class="ml-3 pt-1 fa fa-chevron-down" [ngClass]=" {'fa-chevron-down' : collapsed, 'fa-chevron-up' : !collapsed}"></i>
        </button>
      </div>
      <div *ngIf="!collapsed">
        <div class="card-body pt-0">
          <ng-content select="card-body"></ng-content>
        </div>
      </div>
    </div>
  `
})
export class CardCollapsibleComponent implements OnInit {

  @Input() collapsed = true;
  @Input() alternateHeader = false;
  ngOnInit(): void {
    //
  }

}
