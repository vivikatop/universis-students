import {NgModule, CUSTOM_ELEMENTS_SCHEMA, OnInit} from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';

import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {FormsModule} from '@angular/forms';
import {ConfigurationService} from './services/configuration.service';
import {ModalService} from './services/modal.service';
import {MsgboxComponent} from './msgbox/msgbox.component';
import {LocalizedDatePipe} from './localized-date.pipe';
import {BsDropdownModule} from 'ngx-bootstrap/dropdown';
import {LangComponent} from './lang-component';
import {ModalModule} from 'ngx-bootstrap';
import {DialogComponent} from './modals/dialog.component';
import { SpinnerComponent } from './modals/spinner.component';
import { NgSpinKitModule } from 'ng-spin-kit';
import { SemesterPipe } from './semester.pipe';
import {RouterModule} from '@angular/router';
import {AppSidebarService} from './services/app-sidebar.service';
import {async} from 'rxjs/internal/scheduler/async';
import {environment} from '../../environments/environment';
import { CardCollapsibleComponent } from './card-collapsible/card-collapsible.component';



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    TranslateModule,
    BsDropdownModule,
    ModalModule,
    NgSpinKitModule,
    RouterModule
  ],
  providers: [
    ConfigurationService,
    ModalService
  ],
  declarations: [
    LocalizedDatePipe,
    MsgboxComponent,
    LangComponent,
    DialogComponent,
    SpinnerComponent,
    SemesterPipe,
    CardCollapsibleComponent
  ],
  entryComponents: [
    DialogComponent,
    SpinnerComponent
  ],
  exports: [
    MsgboxComponent,
    LangComponent,
    DialogComponent,
    LocalizedDatePipe,
    SemesterPipe,
    CardCollapsibleComponent
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class SharedModule  implements OnInit {

  constructor(private _translateService: TranslateService) {}
  async ngOnInit() {
    // create promises chain
    const sources = environment.languages.map(async (language) => {
      const translations = await import(`../../assets/i18n/${language}.json`);
      this._translateService.setTranslation(language, translations, true);
    });
    // execute chain
    await Promise.all(sources);
  }

}

