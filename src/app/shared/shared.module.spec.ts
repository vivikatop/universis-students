import { SharedModule } from './shared.module';
import {async, TestBed} from '@angular/core/testing';
import {CommonModule} from '@angular/common';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {ConfigurationService} from './services/configuration.service';
import {TestingConfigurationService} from '../test';
import {MostModule} from '@themost/angular';

describe('SharedModule', () => {
  let sharedModule: SharedModule;

  beforeEach(async(() => {
    return TestBed.configureTestingModule({
      imports: [
        CommonModule,
        HttpClientTestingModule,
        TranslateModule.forRoot(),
        MostModule.forRoot({
          base: '/',
          options: {
            useMediaTypeExtensions: false
          }
        })
      ],
      providers: [
        {
          provide: ConfigurationService,
          useClass: TestingConfigurationService
        }
      ]
    }).compileComponents().then( () => {
      const translateService = TestBed.get(TranslateService);
      sharedModule = new SharedModule(translateService);
    });
  }));

  it('should create an instance', () => {
    expect(sharedModule).toBeTruthy();
  });
});
