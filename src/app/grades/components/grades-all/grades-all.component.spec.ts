import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {GradesService} from '../../services/grades.service';
import {GradeScaleService} from '../../services/grade-scale.service';
import {TranslateModule} from '@ngx-translate/core';
import {NgPipesModule} from 'ngx-pipes';
import {SharedModule} from 'src/app/shared/shared.module';
import {ModalModule, TooltipModule} from 'ngx-bootstrap';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {MostModule} from '@themost/angular';
import {ConfigurationService} from 'src/app/shared/services/configuration.service';

import {GradesAllComponent} from './grades-all.component';
import {GradesStatboxComponent} from '../grades-statbox/grades-statbox.component';
import {TestingConfigurationService} from '../../../test';
import {ErrorModule} from '../../../error/error.module';
import {RouterModule} from '@angular/router';
import {APP_BASE_HREF} from '@angular/common';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';

describe('GradesAllComponent', () => {
    let component: GradesAllComponent;
    let fixture: ComponentFixture<GradesAllComponent>;

    beforeEach(async(() => {
        return TestBed.configureTestingModule({
            declarations: [GradesAllComponent, GradesStatboxComponent],
            providers: [
                GradesService,
                GradeScaleService,
                {
                    provide: ConfigurationService,
                    useClass: TestingConfigurationService
                },
                {
                    provide: APP_BASE_HREF,
                    useValue: '/'
                }
            ],
            imports: [
                HttpClientTestingModule,
                TranslateModule.forRoot(),
                MostModule.forRoot({
                    base: '/',
                    options: {
                        useMediaTypeExtensions: false
                    }
                }),
                SharedModule,
                NgPipesModule,
                ModalModule.forRoot(),
                TooltipModule.forRoot()
            ],
          schemas: [
            CUSTOM_ELEMENTS_SCHEMA
          ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(GradesAllComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
