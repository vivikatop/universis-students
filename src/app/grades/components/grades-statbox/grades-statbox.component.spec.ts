import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ConfigurationService } from 'src/app/shared/services/configuration.service';
import { TranslateModule } from '@ngx-translate/core';
import { GradesService } from '../../services/grades.service';
import { GradeScaleService } from '../../services/grade-scale.service';

import { GradesStatboxComponent } from './grades-statbox.component';
import {TestingConfigurationService} from '../../../test';
import {MostModule} from '@themost/angular';

describe('GradesStatboxComponent', () => {
  let component: GradesStatboxComponent;
  let fixture: ComponentFixture<GradesStatboxComponent>;

  beforeEach(async(() => {
    return TestBed.configureTestingModule({
      declarations: [ GradesStatboxComponent ],
      imports: [ HttpClientTestingModule,
          TranslateModule.forRoot(),
          MostModule.forRoot({
              base: '/',
              options: {
                  useMediaTypeExtensions: false
              }
          })],
      providers: [
        GradesService,
        GradeScaleService,
          {
              provide: ConfigurationService,
              useClass: TestingConfigurationService
          }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GradesStatboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
