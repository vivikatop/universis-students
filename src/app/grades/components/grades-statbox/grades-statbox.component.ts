import { Component, OnInit, Input } from '@angular/core';
import {AngularDataContext} from "@themost/angular";
import {GradesService} from "../../services/grades.service";

@Component({
  selector: 'app-grades-statbox',
  templateUrl: './grades-statbox.component.html',
  styleUrls: ['./grades-statbox.component.scss']
})
export class GradesStatboxComponent implements OnInit {

  @Input() title: string;
  @Input() statsPeriod: string;
  @Input() registeredCourses: number;
  @Input() passedCourses: number;
  @Input() failedCourses: number;
  @Input() passedGradeAverage: string;

  constructor(private _contextService: AngularDataContext, private _gradeService: GradesService) { }

  ngOnInit() {
      //
  }

}
