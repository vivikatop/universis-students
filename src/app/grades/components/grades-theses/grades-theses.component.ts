import { Component, OnInit } from '@angular/core';
import {GradesService} from '../../services/grades.service';
import {LoadingService} from '../../../shared/services/loading.service';

@Component({
  selector: 'app-grades-theses',
  templateUrl: './grades-theses.component.html',
  styleUrls: ['./grades-theses.component.scss']
})
export class GradesThesesComponent implements OnInit {

  public thesisInfo: any;
  public isLoading = true;

  constructor(private gradesService: GradesService,
              private loadingService: LoadingService) {
    this.loadingService.showLoading();
    this.gradesService.getThesisInfo().then((res) => {
      this.thesisInfo = res.value;
      this.loadingService.hideLoading();
      this.isLoading = false;
    }).catch( err => {
      this.loadingService.hideLoading();
      this.isLoading = false;
      console.log(err);
    });
  }

  ngOnInit() {
  }

}
