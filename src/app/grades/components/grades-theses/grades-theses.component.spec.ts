import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {BsModalRef, CollapseModule, ModalModule, TooltipModule} from 'ngx-bootstrap';
import {GradesService} from '../../services/grades.service';
import {GradeScaleService} from '../../services/grade-scale.service';
import {TranslateModule} from '@ngx-translate/core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {ConfigurationService} from 'src/app/shared/services/configuration.service';

import {GradesThesesComponent} from './grades-theses.component';
import {TestingConfigurationService} from '../../../test';
import {MostModule} from '@themost/angular';
import {SharedModule} from '../../../shared/shared.module';
import {RouterTestingModule} from '@angular/router/testing';
import {NgPipesModule} from 'ngx-pipes';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';

describe('GradesThesesComponent', () => {
    let component: GradesThesesComponent;
    let fixture: ComponentFixture<GradesThesesComponent>;

    beforeEach(async(() => {
        return TestBed.configureTestingModule({
            declarations: [GradesThesesComponent],
            providers: [
              BsModalRef,
              GradesService,
              GradeScaleService,
              {
                  provide: ConfigurationService,
                  useClass: TestingConfigurationService
              }
            ],
            imports: [
              HttpClientTestingModule,
              RouterTestingModule,
              TranslateModule.forRoot(),
              MostModule.forRoot({
                  base: '/',
                  options: {
                      useMediaTypeExtensions: false
                  }
              }),
              ModalModule.forRoot(),
              SharedModule
            ],
          schemas: [
            CUSTOM_ELEMENTS_SCHEMA
          ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(GradesThesesComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
