import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {TranslateModule} from '@ngx-translate/core';
import {CollapseModule} from 'ngx-bootstrap';
import {ModalModule} from 'ngx-bootstrap';
import {SharedModule} from 'src/app/shared/shared.module';
import {GradesService} from '../../services/grades.service';
import {GradeScaleService} from '../../services/grade-scale.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AngularDataContext, DATA_CONTEXT_CONFIG, MostModule} from '@themost/angular';
import {ConfigurationService} from 'src/app/shared/services/configuration.service';
import {APP_INITIALIZER, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';

import {GradesStatboxComponent} from '../grades-statbox/grades-statbox.component';
import {GradesRecentComponent} from './grades-recent.component';
import {TestingConfigurationService} from '../../../test';

describe('GradesRecentComponent', () => {
    let component: GradesRecentComponent;
    let fixture: ComponentFixture<GradesRecentComponent>;

    beforeEach(async(() => {
        return TestBed.configureTestingModule({
            declarations: [GradesRecentComponent, GradesStatboxComponent],
            providers: [
                GradesService,
                GradeScaleService,
                {
                    provide: ConfigurationService,
                    useClass: TestingConfigurationService
                }
            ],
            imports: [
                TranslateModule.forRoot(),
                MostModule.forRoot({
                    base: '/',
                    options: {
                        useMediaTypeExtensions: false
                    }
                }),
                CollapseModule,
                HttpClientTestingModule,
                ModalModule.forRoot(),
                SharedModule
            ],
          schemas: [
            CUSTOM_ELEMENTS_SCHEMA
          ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(GradesRecentComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
