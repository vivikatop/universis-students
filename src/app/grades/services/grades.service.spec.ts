import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import {AngularDataContext, DATA_CONTEXT_CONFIG, MostModule} from '@themost/angular';
import { ConfigurationService } from 'src/app/shared/services/configuration.service';
import { APP_INITIALIZER } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { GradesService } from './grades.service';
import { GradeScaleService } from './grade-scale.service';
import {TestingConfigurationService} from '../../test';

describe('GradesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        GradesService,
        GradeScaleService,
          {
              provide: ConfigurationService,
              useClass: TestingConfigurationService
          }],
      imports: [HttpClientTestingModule,
          TranslateModule.forRoot(),
          MostModule.forRoot({
              base: '/',
              options: {
                  useMediaTypeExtensions: false
              }
          })]
    });
  });

  it('should be created', inject([GradesService], (service: GradesService) => {
    expect(service).toBeTruthy();
  }));
});
