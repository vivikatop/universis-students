import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import { GradesRoutingModule } from './grades-routing.module';
import {environment} from '../../environments/environment';
import { GradesRecentComponent } from './components/grades-recent/grades-recent.component';
import { GradesAllComponent } from './components/grades-all/grades-all.component';
import {GradesHomeComponent} from "./components/grades-home/grades-home.component";
import { GradesStatboxComponent } from './components/grades-statbox/grades-statbox.component';
import {CollapseModule} from 'ngx-bootstrap';
import { GradesThesesComponent } from './components/grades-theses/grades-theses.component';
import {FormsModule} from "@angular/forms";
import {NgPipesModule} from "ngx-pipes";
import {GradesSharedModule} from "./grades-shared.module";
import {SharedModule} from '../shared/shared.module';
import {TooltipModule} from 'ngx-bootstrap';
import {CardCollapsibleComponent} from '../shared/card-collapsible/card-collapsible.component';

@NgModule({
  imports: [
      FormsModule,
    CommonModule,
      NgPipesModule,
    GradesRoutingModule,
    TranslateModule,
    CollapseModule.forRoot(),
      GradesSharedModule,
    SharedModule,
    TooltipModule
  ],
  declarations: [
    GradesRecentComponent,
    GradesAllComponent,
    GradesHomeComponent,
    GradesStatboxComponent,
    GradesThesesComponent
  ],
  exports: [
    TooltipModule
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class GradesModule {
  constructor(private _translateService: TranslateService) {
    environment.languages.forEach((culture) => {
      import(`./i18n/grades.${culture}.json`).then((translations) => {
        this._translateService.setTranslation(culture, translations, true);
      });
    });
  }
}
