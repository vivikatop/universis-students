import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegistrationsHomeComponent } from './components/registrations-home/registrations-home.component';
import { RegistrationListComponent } from './components/registrations-list/registrations-list.component';
import { RegistrationSemesterComponent } from './components/registrations-semester/registrations-semester.component';
import { RegistrationCoursesComponent } from './components/registrations-courses/registrations-courses.component';
import { CurrentRegistrationService } from './services/currentRegistrationService.service';
import {FormsModule} from '@angular/forms';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {HttpClientModule} from '@angular/common/http';
import {MostModule} from '@themost/angular';
import {RegistrationsRoutingModule} from './registrations-routing.routing';
import { environment } from '../../environments/environment';
import { SharedModule } from '../shared/shared.module';
import {ProfileSharedModule} from "../profile/profile-shared.module";
import { RegistrationCheckoutComponent } from './components/registration-checkout/registration-checkout.component';
import { NgPipesModule} from 'ngx-pipes';
import {TooltipModule} from 'ngx-bootstrap/tooltip';
import { RegistrationSpecialtyComponent } from './components/registration-specialty/registration-specialty.component';
import { ModalModule } from 'ngx-bootstrap/modal';

@NgModule({
  imports: [
    HttpClientModule,
    CommonModule,
    FormsModule,
    TranslateModule,
    MostModule,
    RegistrationsRoutingModule,
    ProfileSharedModule,
    SharedModule,
    NgPipesModule,
    TooltipModule,
    ModalModule.forRoot()
  ],
    entryComponents: [
        RegistrationSpecialtyComponent
    ],
  declarations: [
    RegistrationsHomeComponent,
    RegistrationListComponent,
    RegistrationSemesterComponent,
    RegistrationCoursesComponent,
    RegistrationCheckoutComponent,
    RegistrationSpecialtyComponent
  ],
    exports: [
        RegistrationSpecialtyComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    providers: [
        CurrentRegistrationService
    ]
})
export class RegistrationsModule {
    constructor(private _translateService: TranslateService) {
        environment.languages.forEach((culture) => {
            import(`./i18n/registrations.${culture}.json`).then((translations) => {
                this._translateService.setTranslation(culture, translations, true);
            });
        });
    }
}
