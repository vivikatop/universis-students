import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { ConfigurationService } from 'src/app/shared/services/configuration.service';
import { BsModalRef } from 'ngx-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CurrentRegistrationService } from '../../services/currentRegistrationService.service';
import { MostModule} from '@themost/angular';
import { ProfileService } from '../../../profile/services/profile.service';
import { RouterTestingModule } from '@angular/router/testing';

import { RegistrationSpecialtyComponent } from './registration-specialty.component';
import {TestingConfigurationService} from '../../../test';

describe('RegistrationSpecialtyComponent', () => {
    // tslint:disable-next-line:prefer-const
  let component: RegistrationSpecialtyComponent;
    // tslint:disable-next-line:prefer-const
  let fixture: ComponentFixture<RegistrationSpecialtyComponent>;

  beforeEach(async(() => {
    return TestBed.configureTestingModule({
      declarations: [ RegistrationSpecialtyComponent ],
      imports: [ HttpClientTestingModule,
          RouterTestingModule,
          FormsModule,
          TranslateModule.forRoot(),
          MostModule.forRoot({
              base: '/',
              options: {
                  useMediaTypeExtensions: false
              }
          })],
      providers: [
        BsModalRef,
          {
              provide: ConfigurationService,
              useClass: TestingConfigurationService
          },
        CurrentRegistrationService,
        ProfileService
        ]
    })
    .compileComponents();
  }));

});
