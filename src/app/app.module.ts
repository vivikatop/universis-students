import { BrowserModule } from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import {APP_INITIALIZER, CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import { AppComponent } from './app.component';
import { FullLayoutComponent } from './shared/layouts/full-layout.component';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {LocationStrategy, HashLocationStrategy} from '@angular/common';
import { MostModule } from '@themost/angular';
import {AngularDataContext, DATA_CONTEXT_CONFIG} from '@themost/angular';
import {ConfigurationService} from './shared/services/configuration.service';
import {SharedModule} from './shared/shared.module';
import {ErrorModule} from './error/error.module';
import {AuthModule} from './auth/auth.module';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {AppSidebarModule} from '@coreui/angular';
// Routing Module
import {AppRoutingModule} from './app.routing';
import {BreadcrumbsComponent} from './shared/layouts/breadcrumbs.component';
import { ProfileService } from './profile/services/profile.service';

import { TooltipModule } from 'ngx-bootstrap/tooltip';


@NgModule({
  declarations: [
    AppComponent,
    FullLayoutComponent,
    BreadcrumbsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    TranslateModule.forRoot(),
    MostModule,
    SharedModule,
    AuthModule,
    AppRoutingModule,
    ErrorModule.forRoot(),
    BsDropdownModule.forRoot(),
    ModalModule.forRoot(),
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    TooltipModule.forRoot(),
    AppSidebarModule
  ],
  providers: [
      {
          provide: DATA_CONTEXT_CONFIG, useValue: {
              base: '/',
              options: {
                  useMediaTypeExtensions: false
              }
          }
      },
    AngularDataContext,
    {
            provide: APP_INITIALIZER,
            useFactory: (config: ConfigurationService) => () => config.load(),
            deps: [ConfigurationService],
            multi: true
    },
    {
            provide: LocationStrategy,
            useClass: HashLocationStrategy
    },
    ProfileService
    ],
  bootstrap: [AppComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})
export class AppModule {
  constructor(private _translateService: TranslateService, private _configurationService: ConfigurationService) {
      //
    }
}
