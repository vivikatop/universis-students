import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ComponentLoaderFactory, PositioningService, TooltipConfig, TooltipModule} from 'ngx-bootstrap';
import {ProfileService} from 'src/app/profile/services/profile.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {MostModule} from '@themost/angular';
import {ConfigurationService} from 'src/app/shared/services/configuration.service';
import {TranslateModule} from '@ngx-translate/core';

import {ProgressBarDegreeComponent} from './progress-bar-degree.component';
import {GradesService} from 'src/app/grades/services/grades.service';
import {GradeScaleService} from 'src/app/grades/services/grade-scale.service';
import {TestingConfigurationService} from '../../../test';

describe('ProgressBarDegreeComponent', () => {
    let component: ProgressBarDegreeComponent;
    let fixture: ComponentFixture<ProgressBarDegreeComponent>;

    beforeEach(async(() => {
        return TestBed.configureTestingModule({
            declarations: [ProgressBarDegreeComponent],
            providers: [
              ComponentLoaderFactory,
              PositioningService,
              TooltipConfig,
              ProfileService,
              GradesService,
              GradeScaleService,
              {
                  provide: ConfigurationService,
                  useClass: TestingConfigurationService
              }
            ],
            imports: [TooltipModule,
                HttpClientTestingModule,
                TranslateModule.forRoot(),
                MostModule.forRoot({
                    base: '/',
                    options: {
                        useMediaTypeExtensions: false
                    }
                })]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ProgressBarDegreeComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
