import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {GradesService} from 'src/app/grades/services/grades.service';
import {GradeScaleService} from 'src/app/grades/services/grade-scale.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {MostModule} from '@themost/angular';
import {ConfigurationService} from 'src/app/shared/services/configuration.service';
import {TranslateModule} from '@ngx-translate/core';

import {StudentRecentGradesComponent} from './student-recent-grades.component';
import {TestingConfigurationService} from '../../../test';

describe('StudentRecentGradesComponent', () => {
    let component: StudentRecentGradesComponent;
    let fixture: ComponentFixture<StudentRecentGradesComponent>;

    beforeEach(async(() => {
        return TestBed.configureTestingModule({
            declarations: [StudentRecentGradesComponent],
            imports: [HttpClientTestingModule,
                TranslateModule.forRoot(),
                MostModule.forRoot({
                    base: '/',
                    options: {
                        useMediaTypeExtensions: false
                    }
                })],
            providers: [
                GradesService,
                GradeScaleService,
                {
                    provide: ConfigurationService,
                    useClass: TestingConfigurationService
                }]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(StudentRecentGradesComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
