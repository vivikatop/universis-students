import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {TranslateModule} from '@ngx-translate/core';
import {MostModule} from '@themost/angular';
import {ConfigurationService} from 'src/app/shared/services/configuration.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';

import {StudentRecentCoursesComponent} from './student-recent-courses.component';
import {TestingConfigurationService} from '../../../test';

describe('StudentRecentCoursesComponent', () => {
    let component: StudentRecentCoursesComponent;
    let fixture: ComponentFixture<StudentRecentCoursesComponent>;

    beforeEach(async(() => {
        return TestBed.configureTestingModule({
            declarations: [StudentRecentCoursesComponent],
            imports: [TranslateModule.forRoot(),
                HttpClientTestingModule,
                MostModule.forRoot({
                    base: '/',
                    options: {
                        useMediaTypeExtensions: false
                    }
                })],
            providers: [
                {
                    provide: ConfigurationService,
                    useClass: TestingConfigurationService
                }]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(StudentRecentCoursesComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
