import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgressBarSemesterComponent } from './progress-bar-semester.component';

describe('ProgressBarSemesterComponent', () => {
  let component: ProgressBarSemesterComponent;
  let fixture: ComponentFixture<ProgressBarSemesterComponent>;

  beforeEach(async(() => {
    return TestBed.configureTestingModule({
      declarations: [ ProgressBarSemesterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgressBarSemesterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
