import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthGuard} from '../auth/guards/auth.guard';
import {RequestsHomeComponent} from './components/requests-home/requests-home.component';
import {RequestsListComponent} from './components/requests-list/requests-list.component';
import {RequestsNewComponent} from './components/requests-new/requests-new.component';

const routes: Routes = [

  {
    path: '',
    component: RequestsHomeComponent,
    canActivate: [
      AuthGuard
    ],
    children: [
      {
        path: '',
        redirectTo: 'list'
      },
      {
        path: 'list',
        component: RequestsListComponent
      },
      {
        path: 'new',
        component: RequestsNewComponent
      }
    ]
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RequestsRoutingModule { }
