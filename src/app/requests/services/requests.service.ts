import { Injectable } from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ConfigurationService} from '../../shared/services/configuration.service';



@Injectable()
export class RequestsService {

  constructor(private _context: AngularDataContext, private _configuration: ConfigurationService) {
    //
  }

   getDocumentTypes() {
    return this._context.model('DocumentConfigurations')
      .asQueryable()
      .where('inLanguage')
      .equal(this._configuration.getCurrentLang())
      .getItems();
   }
  getActiveRequests() {
    return this._context.model('RequestDocumentActions')
      .asQueryable()
      .where('actionStatus/alternateName')
      .equal('ActiveActionStatus')
      .select('object/name as requestActionName', 'object/alternateName as alternateName',
          'actionStatus/alternateName as status', 'description', 'dateCreated')
      .expand('actionStatus', 'object')
      .getItems();
  }

  getDocumentRequests() {
    return this._context.model('RequestDocumentActions')
      .asQueryable()
      .expand('object', 'actionStatus, messages($orderby=dateCreated desc;$expand=attachments)')
      .orderByDescending('dateCreated')
      .getItems();
  }
  getMessageRequests() {
    return this._context.model('RequestMessageActions')
      .asQueryable()
      .expand('actionStatus, messages($orderby=dateCreated desc;$expand=attachments)')
      .orderByDescending('dateCreated')
      .getItems();
  }

  downloadFile(attachments) {
    const headers = new Headers();
    const serviceHeaders = this._context.getService().getHeaders();
    Object.keys(serviceHeaders).forEach((key) => {
      if (serviceHeaders.hasOwnProperty(key)) {
        headers.set(key, serviceHeaders[key]);
      }
    });
    const attachURL = attachments[0].url.replace(/\\/g, '/').replace('/api', '');

    const fileURL = this._context.getService().resolve(attachURL);
    fetch(fileURL, {
      headers: headers,
      credentials: 'include'
    }).then((response) => {

      return response.blob();
    })
      .then(blob => {

        const objectUrl = window.URL.createObjectURL(blob);
        const a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = objectUrl;
        a.download = `${attachments[0].name}`;
        a.click();
        window.URL.revokeObjectURL(objectUrl);
        a.remove();
      });
  }

}
