import { Component, OnInit } from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {Router, ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-requests-new',
  templateUrl: './requests-new.component.html',
  styleUrls: ['./requests-new.component.scss']
})
export class RequestsNewComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private _router: Router,
    private _context: AngularDataContext) {
    this.model.alternateName = this.route.snapshot.queryParamMap.get('type');
    this.model.object.alternateName = this.route.snapshot.queryParamMap.get('type');
  }

  public model = {
    name: null,
    alternateName: null,
    description: null,
    object: {
      alternateName: null
    }
  };

  ngOnInit() {
    //
  }

  submit() {
    const action = (this.model.alternateName == 'OtherRequest') ? 'RequestMessageActions' : 'RequestDocumentActions';
    if (action == 'RequestMessageActions') {
      this.model.object = null;
    }
    this._context.model(action)
        .save(this.model)
        .then(() => {
          return this._router.navigate(['/requests/list']);
        }).catch(err => {
      console.log(Error);
    });
  }}
