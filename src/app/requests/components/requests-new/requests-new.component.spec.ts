import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AngularDataContext, DATA_CONTEXT_CONFIG, MostModule} from '@themost/angular';
import {ConfigurationService} from 'src/app/shared/services/configuration.service';
import {APP_INITIALIZER} from '@angular/core';
import {TranslateModule} from '@ngx-translate/core';
import {RouterTestingModule} from '@angular/router/testing';
import {FormsModule} from '@angular/forms';

import {RequestsNewComponent} from './requests-new.component';
import {TestingConfigurationService} from '../../../test';
// important: exclude test because of a karma error on linux
// todo: fix error on linux
xdescribe('RequestsNewComponent', () => {
    let component: RequestsNewComponent;
    let fixture: ComponentFixture<RequestsNewComponent>;

    beforeEach(async(() => {
        return TestBed.configureTestingModule({
            declarations: [
                RequestsNewComponent
            ],
            imports: [
                HttpClientTestingModule,
                TranslateModule.forRoot(),
                RouterTestingModule,
                FormsModule,
                MostModule.forRoot({
                    base: '/',
                    options: {
                        useMediaTypeExtensions: false
                    }
                })
            ],
            providers: [
                {
                    provide: ConfigurationService,
                    useClass: TestingConfigurationService
                }
            ],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(RequestsNewComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
