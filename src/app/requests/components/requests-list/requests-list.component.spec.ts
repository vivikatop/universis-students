import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {MostModule} from '@themost/angular';
import {ConfigurationService} from 'src/app/shared/services/configuration.service';
import {TranslateModule} from '@ngx-translate/core';
import {ModalModule} from 'ngx-bootstrap';
import {ToastrModule} from 'ngx-toastr';
import {SharedModule} from 'src/app/shared/shared.module';
import {RouterTestingModule} from '@angular/router/testing';

import {RequestsListComponent} from './requests-list.component';
import {TestingConfigurationService} from '../../../test';

describe('RequestsListComponent', () => {
    let component: RequestsListComponent;
    let fixture: ComponentFixture<RequestsListComponent>;

    beforeEach(async(() => {
        return TestBed.configureTestingModule({
            declarations: [
                RequestsListComponent
            ],
            imports: [
                RouterTestingModule,
                HttpClientTestingModule,
                ModalModule.forRoot(),
                ToastrModule.forRoot(),
                TranslateModule.forRoot(),
                MostModule.forRoot({
                    base: '/',
                    options: {
                        useMediaTypeExtensions: false
                    }
                }),
                SharedModule
            ],
            providers: [
                {
                    provide: ConfigurationService,
                    useClass: TestingConfigurationService
                }
            ],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(RequestsListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
